import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class GameTestIT {

    @Test
    public void shouldVerifyScoreFor2Rounds(){
        ConsoleBehaviour consoleBehaviour1 = new ConsoleBehaviour(new Scanner("1 \n 2"));
        ConsoleBehaviour consoleBehaviour2 = new ConsoleBehaviour(new Scanner("1 \n 1"));

        Player player1 = new Player(consoleBehaviour1);
        Player player2 = new Player(consoleBehaviour2);

        Machine machine = new Machine();

        Game game = new Game(player1, player2, machine);

        game.play(2);

        assertEquals(5,player1.finalScore());
        assertEquals(1,player2.finalScore());
    }


}
