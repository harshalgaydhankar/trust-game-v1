import java.util.Scanner;

public class ConsoleBehaviour implements IPlayerBehaviour {
    private Scanner scanner;

    public ConsoleBehaviour(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public String makeMove() {
        int userInput = scanner.nextInt();
        if(userInput == 1)
            return MoveType.COOPERATE.value;
        else if(userInput == 2)
            return  MoveType.CHEAT.value;
        return null;
    }
}
