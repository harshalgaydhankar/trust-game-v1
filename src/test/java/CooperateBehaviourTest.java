import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class CooperateBehaviourTest {

    @Test
    public void shouldReturnOnlyCooperateMoveforPlayer(){
        CooperateBehaviour cooperateBehaviour = new CooperateBehaviour();
        String moveType = cooperateBehaviour.makeMove();
        assertEquals(MoveType.COOPERATE.value, moveType);
    }

}
