import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheatBehaviourTest {

    @Test
    public void shouldReturnOnlyCheatMoveforPlayer(){
        CheatBehaviour cheatBehaviour = new CheatBehaviour();
        String moveType = cheatBehaviour.makeMove();
        assertEquals(MoveType.CHEAT.value, moveType);
    }
}
