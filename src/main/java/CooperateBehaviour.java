public class CooperateBehaviour implements IPlayerBehaviour {

    @Override
    public String makeMove() {
        return MoveType.COOPERATE.value;
    }
}
