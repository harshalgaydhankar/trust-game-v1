import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class ConsoleBehaviourTest {

    @Test
    public void shouldReturnCooperateMoveforPlayer(){
        ConsoleBehaviour consoleBehaviour = new ConsoleBehaviour(new Scanner("1"));
        String moveType = consoleBehaviour.makeMove();
        assertEquals(MoveType.COOPERATE.value, moveType);
    }

    @Test
    public void shouldReturnCheatMoveforPlayer(){
        ConsoleBehaviour consoleBehaviour = new ConsoleBehaviour(new Scanner("2"));
        String moveType = consoleBehaviour.makeMove();
        assertEquals(MoveType.CHEAT.value, moveType);
    }
}
